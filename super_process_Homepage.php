<?php
include_once("Collection.php");
class Super_Process_HomePage extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
	function getAllListAlbum(){
		$sql="select * from album limit 0 ,10";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	/**
	 * Hàm lấy danh sách radio online
	 *
	 * @param boolean $make_slider  cờ để lấy data làm slider
	 * @return void
	 */
	function getRadioOnline($make_slider = false){
		$limit_data = 10;
		if($make_slider == true){
			$limit_data  = 3;
		}
		$sql="select * from album where category_id in (14,15) limit 0 , $limit_data";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListAlbumLimitByCategory($id_category,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from album where category_id=$id_category";
		}else {
			$sql = "select * from album where category_id=$id_category limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListAlbumLimitByArtist($id_artist,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from album where artist_id=$id_artist";
		}else {
			$sql = "select * from album where artist_id=$id_artist limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListAlbumLimitByTag($id_tag,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from album where tag_id=$id_tag";
		}else {
			$sql = "select * from album where tag_id=$id_tag limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListAlbumMostView($start,$end){
		$sql="select * from album order by rand() desc limit $start,$end";
		$data=$this->getCollectionBySql($sql);
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListAlbumMoreView(){
		$sql="select * from album order by view desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getDetailAlbum($id_album){
		$sql="select * from album where id=$id_album";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function searchAlbum($querySearch){
		$sql="select * from album where name like '%'.$querySearch.'%'";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListSongOfAlbum($idAlbum){
		$sql="select * from song where album_id=$idAlbum";
		$data=$this->getCollectionBySql($sql);
        echo $data;
	}
	function getListAlbumInPerDay(){
		$currentDay=date("Y-m-d");
		$sql="select * from album where date_modifier like '$currentDay' limit 0 ,10";
		$data=$this->getCollectionBySql($sql);
        echo $data;	
	}
	function getAlbumRecent($user_id){
		$sqlGetAlbum="select * from recent where album_id != 0 and user_id='$user_id'";
		$dataAlbum=$this->getCollectionBySql($sqlGetAlbum);
		$idAlbum="";
		foreach($dataAlbum as $_recent){
			$idAlbum.=$_recent->getalbum_id().",";
		}
		$idAlbum=substr($idAlbum,0,strlen($idAlbum)-1);
		if($idAlbum==""){
			$idAlbum=rand (1,1000);
		}
		$sql="select * from album where id in ($idAlbum)";
		$data=$this->getCollectionBySql($sql);
		echo $data;
	}
	function getListAlbumRandom(){
		$currentDay=date("Y-m-d");
		$sql="select * from album order by id desc limit 0 ,10";
		$data=$this->getCollectionBySql($sql);
        echo $data;	
	}
	
	function getAlbumByIds($arrayIds){
		$sql="select * from album where id in ($arrayIds)";
		$data=$this->getCollectionBySql($sql);
		echo $data;
	}
	function getSongByIds($arrayIds){
		$sql="select * from song where id in ($arrayIds)";
		$data=$this->getCollectionBySql($sql);
		echo $data;
	}
	function getListSontByTag($id_tag){
		$sql="select *,album.id as id_album,album.name as name_album,category.name as name_category,artist.name as name_artist from album 
		left join artist on album.artist_id=artist.id 
		left join category on album.category_id=category.id
		where album.tag_id=$id_tag order by rand()   limit 0 , 5 ";
		$data=$this->getCollectionBySql($sql);
        echo $data;
	}
	function getListTag($arrayTags){
		$sql="select * from tag where id in($arrayTags)";
		$data=$this->getCollectionBySql($sql);
        echo $data;
	}
	function subStringLimit($limit,$string){
		echo substr($string,0,$limit)."...";
	}
	function getArtist($id){
		$sql="select * from artist where id=$id";
		$data=$this->getCollectionBySql($sql);
        echo $data;
	}
	function getLikeAlbum($id_album){
		$sql="select id from like_album where album_id=$id_album";
		$data=$this->getCollectionBySql($sql);
		echo $data;
	}
	function checkIsLikeAlbum($id_album,$user_id){
		$sql="select id from like_album where album_id=$id_album and user_id='$user_id'";
		$data=$this->getCollectionBySql($sql);
		echo $data;
	}
	function getCommentOnAlbum($id_album){
		$sql="select content,username from album_comment where album_id=$id_album order by id desc limit 0,15 ";
		$data=$this->getCollectionBySql($sql);
		echo $data;
	}
	function addCommentOnAlbum($text,$user_id,$userName,$album_id){
		$albumData = new Collection("album_comment","id");
		$albumData->setData("id","NULL");
		$albumData->setData("album_id",$album_id);
		$albumData->setData("content",$text);
		$albumData->setData("user_id",$user_id);
		$albumData->setData("username",$userName);
		$albumData->inserRow();
	}
	function addCommentOnSong($text,$user_id,$userName,$song_id){
		$albumData = new Collection("song_comment","id");
		$albumData->setData("id","NULL");
		$albumData->setData("song_id",$song_id);
		$albumData->setData("content",$text);
		$albumData->setData("user_id",$user_id);
		$albumData->setData("username",$userName);
		$albumData->inserRow();
	}
	function updateListen($id_album,$viewCount){
		$viewCount=$viewCount+1;
		$this->setData("id",$id_album);
		$this->setData("view",$viewCount);
		$this->updateRow();
	}

}