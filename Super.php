<?php
/*
This class is super core on this project never remove it
*/
@session_start();
class Super_Core{

	public function Super_Core()
	{

	}
	public static function getModel($tablename,$primarykey,$nameUpperCase)
	{
		$nameClass = "super_process_" . $tablename . ".php";
		include_once("$nameClass");
		$upperCase = "Super_Process_" . $nameUpperCase;
		$objectReflection = new ReflectionClass($upperCase);
		$args = array($tablename, $primarykey);
		$object = $objectReflection->newInstanceArgs($args);
		return $object;
	}
	public  static function getSenderEmail(){
		return "leminhtamboy@gmail.com";
	}
	public static function getHeaderEmail(){
		return Super_Core::getRootWeb()."/Email/header.php";
	}
	public static function getFooterEmail(){
		return Super_Core::getRootWeb()."/Email/footer.html";
	}
	public function subStringLimit($limit,$string){
		return mb_substr($string,0,$limit,"utf-8")."...";
	}
	public function subCommaInString($string){
		$string=substr($string,0,strlen($string)-1);
		return $string;
	}
	public function checkIssetParam($name,$method){
		$isset=false;
		switch ($method) {
			case 'post':
				if(isset($_POST[$name])){
					$isset=true;
				}
				break;
			case 'get':
				if(isset($_GET[$name])){
					$isset=true;
				}
				break;
			case 'request':
				if(isset($_REQUEST[$name])){
					$isset=true;
				}
				break;
			default:
				break;
		}
		return $isset;
	}
	public function getPost($name){
		return $_POST["$name"];
	}
	//unsecurity method for controller
	public function getGet($name){
		return $_GET["$name"];
	}
	public function getRequest($name){
		return $_REQUEST["$name"];
	}
	public function setSession($value,$key_session){
		$_SESSION[$key_session]=$value;
	}
	public function getSession($key_session){
		if($this->checkIssetSession($key_session)){
			return $_SESSION[$key_session];	
		}
		else{
			return null;
		}
	}
	public function checkIssetSession($key_session){
		if(isset($_SESSION[$key_session])){
			return true;
		}
		return false;
	}
	public function destroySession(){
		session_unset();
	}
	public function getRootWeb(){
		return SUPERAI_ROOT;
	}
	public function getImgesPath(){
		return ConfigGlobal::$realPath."/images";
	}
	public function getImgesUrl(){
		return ConfigGlobal::$realPath."/images";	
	}
	public function getWebUrl(){
		return ConfigGlobal::$realPath;
	}
	public function getMediaUrl(){
		return  $this->getWebUrl()."/data/";
	}
	public function getAjaxUrl(){
		return  $this->getWebUrl()."/Ajax/";
	}
	public function getCurrentUrl(){
		return  'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
	public function processStringForInQuery($stringInput){
		$lastIndex=strrpos($stringInput,",");
		$stringInput=substr($stringInput,0,$lastIndex);
		return $stringInput;
	}
	//Image
	public function checkIssetImage($url){
		$file = $url; // 'images/'.$file (physical path)
		if (file_exists($file)) {
		    return true;
		} else {
		    return false;
		}
	}

	public function getArtistBanner(){
		echo $this->getImgesUrl()."/"."artist-default.jpg";
	}
	public function getMusicBanner(){
		echo $this->getImgesUrl()."/"."music-banner-resized.gif";
	}
	public static function ShowError($type){
		switch ($type){
			case "empty":
				return "No data recieve.Review your condition or url or query.If you can not resolve please contact admin.Thank you";
			break;
			case "no-condition":
				return "No Condition on your request . Please check your url or contact admin of API.";
			break;
			case "no-login":
				return "Wrong User Name or Password.Please check your information . Thank you!";
			break;
		}
	}
}
?>