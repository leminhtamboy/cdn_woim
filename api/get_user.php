<?php
include_once("../Super.php");
include_once("../super_process_users.php");
$process = new Super_Process_Users("users","id");
if(isset($_REQUEST["condition"])){
    $condition=$_REQUEST["condition"];
    switch ($condition){
        case "login":
            $user_name=$_REQUEST["user_name"];
            $pass=$_REQUEST["pass"];
            $user=$process->LoginActionByAPI($user_name,$pass);
            if($user=="incorrect"){
                echo Super_Core::showError("no-login");
            }
            break;
        case "get-artist": // lấy tác giả theo id
            $id_artist=$_REQUEST["id_artist"];
            $artist=$process->getInformationArtist($id_artist);
            break;
        default:
            echo Super_Core::showError("no-condition");
            break;
    }
}else{
    echo Super_Core::showError("empty");
}