<?php
include_once("../Super.php");
include_once("../super_process_Homepage.php");
$processHomepage = new Super_Process_HomePage("album","id");
if(isset($_REQUEST["condition"])){
	$condition=$_REQUEST["condition"];
	switch ($condition){
		case "random-album":
			if(!isset($start) && !isset($end)){
				$start=0;
				$end=10;
			}else{
				$start=$_REQUEST["start"];
				$end=$_REQUEST["end"];
			}
			$processHomepage->getListAlbumMostView($start,$end);
			break;
		case "all-album":
			$processHomepage->getAllListAlbum();
			break;
		case "limit-album-category":
			$id_category=$_REQUEST["id_category"];
			$start=$_REQUEST["start"];
			$end=$_REQUEST["end"];
			$processHomepage->getListAlbumLimitByCategory($id_category,$start,$end);
			break;
		case "detail-album":
			$id_album=$_REQUEST["id_album"];
			$processHomepage->getDetailAlbum($id_album);
			break;
		case "limit-album-artist":
			$id_artist=$_REQUEST["id_artist"];
			$start=$_REQUEST["start"];
			$end=$_REQUEST["end"];
			$processHomepage->getListAlbumLimitByArtist($id_artist,$start,$end);
			break;
		case "limit-album-tag":
			$id_tag=$_REQUEST["id_tag"];
			$start=$_REQUEST["start"];
			$end=$_REQUEST["end"];
			$processHomepage->getListAlbumLimitByTag($id_tag,$start,$end);
			break;
		case "search-album":
			$query=$_REQUEST["q"];
			$processHomepage->searchAlbum($query);
			break;
		case "radio-online":
			$processHomepage->getRadioOnline();
			break;
		case "radio-online-slider":
			$processHomepage->getRadioOnline(true);
			break;
		default:
			echo Super_Core::showError("no-condition");
			break;
	}
}else{
	echo Super_Core::showError("empty");
}