<?php
include_once("../Super.php");
include_once("../super_process_song.php");
$process = new Super_Process_Song("song","id");
if(isset($_REQUEST["condition"])){
    $condition=$_REQUEST["condition"];
    switch ($condition){
        case "limit-song-album":
            $id_album=$_REQUEST["id_album"];
            $start=$_REQUEST["start"];
            $end=$_REQUEST["end"];
            $process->getListSongLimitByAlbum($id_album,$start,$end);
            break;
        case "limit-song-category":
            $id_category=$_REQUEST["id_category"];
            $start=$_REQUEST["start"];
            $end=$_REQUEST["end"];
            $process->getListSongLimitByCategory($id_category,$start,$end);
            break;
        case "limit-song-artist":
            $id_artist=$_REQUEST["id_artist"];
            $start=$_REQUEST["start"];
            $end=$_REQUEST["end"];
            $process->getListSongLimitByArtist($id_artist,$start,$end);
            break;
        case "limit-song-tag":
            $id_tag=$_REQUEST["id_tag"];
            $start=$_REQUEST["start"];
            $end=$_REQUEST["end"];
            $process->getListSongLimitByTag($id_tag,$start,$end);
            break;
        case "detail-song":
            $id_song=$_REQUEST["id_song"];
            $process->getDetailSong($id_song);
            break;
        case "search-song":
            $query=$_REQUEST["q"];
            $process->searchSong($query);
            break;
        default:
            echo Super_Core::showError("no-condition");
            break;
    }
}else{
    echo Super_Core::showError("empty");
}