<?php
include_once("Collection.php");
class Super_Process_Song extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
	function getListSongLimitByCategory($id_category,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from song where category_id=$id_category";
		}else {
			$sql = "select * from song where category_id=$id_category limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListSongLimitByAlbum($id_album,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from song where album_id=$id_album";
		}else {
			$sql = "select * from song where album_id=$id_album limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListSongLimitByArtist($id_artist,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from song where artist_id=$id_artist";
		}else {
			$sql = "select * from song where artist_id=$id_artist limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
	function getListSongLimitByTag($id_tag,$start,$end){
		if($start==0  && $end ==0){
			$sql = "select * from song where tag_id=$id_tag";
		}else {
			$sql = "select * from song where tag_id=$id_tag limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
    function getDetailSong($id_song){
    	$sql="select * from song where id=$id_song";
    	$data=$this->getCollectionBySql($sql);
        return json_encode($data,JSON_UNESCAPED_UNICODE);
    }
	function searchSong($querySearch){
		$sql="select * from song where name like '%'.$querySearch.'%'";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
	}
    function getLimitListenSong($id_user){
		$sql="select * from listen_album where user_id='$id_user'";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListMenu(){
		$sql="select * from category where parent=0";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getChildrenMenu($parent_id){
		$sql="select * from category where parent=$parent_id";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getDetailCategory($id_category){
		$sql="select * from category where id=$id_category";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function updateListen($id_song,$viewCount){
		$viewCount=$viewCount+1;
		$this->setData("id",$id_song);
		$this->setData("view",$viewCount);
		$this->updateRow();
	}
	function getAlbumInCategory($id){
		$sql="select * from album where category_id=$id order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongInCategory($id_category){
		$sql="select * from song where category_id=$id_category order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getArtistBySongId($artist_id){
		$sql="select * from artist where id=$artist_id";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumOfSong($album_id){
		$sql="select name,id from album where id=$album_id";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumByArtist($artist_id){
		$sql="select * from album where artist_id=$artist_id limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongByArtist($artist_id){
		$sql="select * from song where artist_id=$artist_id limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getCommentOnSong($song_id){
		$sql="select content,username from song_comment where song_id=$song_id  order by id desc limit 0,15";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSuggestedSong(){
		//TODO
	}
}