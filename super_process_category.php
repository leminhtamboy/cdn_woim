<?php
include_once("Collection.php");
class Super_Process_Category extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
	function getListMenu(){
		$sql="select * from category where parent=0";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getListCateogry(){
		$sql="select * from `category`";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
	}
	function getListTag(){
		$sql="select * from `tag`";
		$data=$this->getCollectionBySql($sql);
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
	}
	function getChildrenMenu($parent_id){
		$sql="select * from category where parent=$parent_id";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getDetailCategory($id_category){
		$sql="select * from category where id=$id_category";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getAlbumInCategory($id){
		$sql="select * from album where category_id=$id order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongInCategory($id_category){
		$sql="select * from song where category_id=$id_category order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumBySearch($key_search){
		$sql="select * from album where name like '%$key_search%' limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongBySearch($key_search){
		$sql="select * from song where name like '%$key_search%' limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
}